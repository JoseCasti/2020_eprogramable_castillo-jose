/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include "string.h"

/*==================[macros and definitions]=================================*/


/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros). */

typedef struct{
		uint8_t nombre[12];
		uint8_t apellido[20];
		uint8_t edad;
	} alumno;


/*==================[internal functions declaration]=========================*/

int main(void)
{
	alumno alu_1; //definicion de variable de tipo alumno y carga de datos
	strcpy(alu_1.nombre, "Jose");
	strcpy(alu_1.apellido,"Castillo");
	alu_1.edad=24;

	alumno alu_2, *al_Ptr; //definicion de variable de tipo alumno, puntero a variable de tipo alumno y carga de datos mediante puntero
	al_Ptr=&alu_2;
	strcpy(al_Ptr->apellido,"Daniel");
	strcpy(al_Ptr->nombre,"Gonzalez");
	al_Ptr->edad=20;

	printf("Nombre: %s \r", alu_1.nombre);
	printf("Apellido: %s \r", alu_1.apellido);
	printf("Edad: %d \r", alu_1.edad);
	printf("Nombre: %s \r", al_Ptr->nombre);
	printf("Apellido: %s \r", al_Ptr->apellido);
	printf("Edad: %d \r", al_Ptr->edad);



	return 0;
}

/*==================[end of file]============================================*/


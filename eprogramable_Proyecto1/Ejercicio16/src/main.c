/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/

/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
cargue cada uno de los bytes de la variable de 32 bits.
b. Realice el mismo ejercicio, utilizando la definición de una “union”.*/


#define BYTE 8

union prueba{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	} todos_los_bytes;

/*==================[internal functions declaration]=========================*/

int main(void)
{
    uint32_t var_Ini=0x01020304;
    uint8_t var_1,var_2,var_3,var_4;

    var_4=var_Ini>>(3*BYTE); //desplazamiento de 24 bits y truncamiento
    var_3=var_Ini>>(2*BYTE); //desplazamiento de 16 bits y truncamiento
    var_2=var_Ini>>BYTE; //desplazamiento de 8 bits y truncamiento
    var_1=var_Ini; //truncamiento 8 bits menos significativos

    printf("Numero HEX01020304 en decimal: %d \r", var_Ini);
    printf("Numero HEX01020304 tomado de a 8 bits en decimal: %d ", var_4);
    printf("%d ", var_3);
    printf("%d ", var_2);
    printf("%d ", var_1);


    /*Mediante union*/

    printf("\r Y mediante union: ");

    todos_los_bytes.byte4 = var_Ini>>(3*BYTE);
    printf("\r %d ", todos_los_bytes.byte4 );
    todos_los_bytes.byte3 = var_Ini>>(2*BYTE);
    printf("\r %d ", todos_los_bytes.byte3 );
    todos_los_bytes.byte2 = var_Ini>>BYTE;
    printf("\r %d ", todos_los_bytes.byte2 );
    todos_los_bytes.byte1 = var_Ini;
    printf("\r %d ", todos_los_bytes.byte1 );


	return 0;
}

/*==================[end of file]============================================*/


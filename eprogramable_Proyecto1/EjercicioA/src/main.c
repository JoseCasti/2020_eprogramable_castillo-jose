/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de ciclos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
} my_leds;
Use como guía para la implementación el siguiente diagrama de flujo: */


typedef struct{
	uint8_t n_led;    //indica el número de led a controlar
	uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;  //indica el tiempo de cada ciclo
	uint8_t mode;     //ON, OFF, TOGGLE
	}leds;

typedef enum{
	ON=1,
	OFF,
	TOGGLE,
} state_t;

typedef enum{
	led_1=1,
	led_2,
	led_3,
} led_t;


void led_control(leds *ptr_Led);


/*==================[internal functions declaration]=========================*/

void led_control (leds *ptr_Led){
	uint8_t i=0;
	switch (ptr_Led->mode)
	{
		case ON:
		   { switch (ptr_Led->n_led)
			 	 {
				case led_1:
					printf("Se enciende %d",led_1);
				break;
				case led_2:
					printf("Se enciende %d",led_2);
				break;
				case led_3:
					printf("Se enciende %d",led_3);
				break;
			 	 }
		   }
		break;
		case OFF:
			{ switch (ptr_Led->n_led)
				{
				case led_1:
					printf("Se apaga %d",led_1);
				break;
				case led_2:
					printf("Se apaga %d",led_2);
				break;
				case led_3:
					printf("Se apaga %d",led_3);
				break;
				}
			}
		break;
		case TOGGLE:
			{
			for (i=0; i<ptr_Led->n_ciclos; i++)
				{
				switch (ptr_Led->n_led)
					{
					case led_1:
						printf("Toggle a %d",led_1);
					break;
					case led_2:
						printf("Toggle a %d",led_2);
					break;
					case led_3:
						printf("Toggle a %d",led_3);
					break;
					}
				for (i=0; i<ptr_Led->periodo ; i++) {}
				}
			}
		break;
		}
}


int main(void)
{
	leds var_Led1={led_1,50,200, ON};
	led_control(&var_Led1);

	printf("\r");

	leds var_Led2={led_2,20,200, OFF};
	led_control(&var_Led2);

	printf("\r");

	leds var_Led3={led_3,10,20, TOGGLE};
	led_control(&var_Led3);


	return 0;
}

/*==================[end of file]============================================*/


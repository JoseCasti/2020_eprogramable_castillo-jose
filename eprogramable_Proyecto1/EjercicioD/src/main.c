/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
/*Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
// uint8_t port; !< GPIO port number
// uint8_t pin; !< GPIO pin number
// uint8_t dir; < GPIO direction ‘0’ IN; ‘1’ OUT
// } gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.   */

#define PUERTO1 1
#define PIN4 4
#define PIN5 5
#define PIN6 6
#define PUERTO2 2
#define PIN14 14
#define SALIDA 1

void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ); //Funcion del anterior ejercicio necesaria para cargar el vector

typedef struct
	{
	uint8_t port; /*!< GPIO port number */
	uint8_t pin;  /*!< GPIO pin number */
	uint8_t dir;  /*!< GPIO direction ‘0’ IN; ‘1’ OUT */
	} gpioConf_t;

void BcdToLcd(uint8_t dig_Bcd , gpioConf_t *config);


/*==================[internal functions declaration]=========================*/
//Funcion del anterior ejercicio necesaria para cargar el vector
void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){
	uint8_t i=0;
	for (i=0; i<digits; i++)
	{
		printf("Digito Nª:%d \n",i);
		bcd_number[i]=data%10;  						// me quedo con el resto de data/10, cuando i=0, para cargarlo en el arreglo
		printf("El valor es: %d \r \n",bcd_number[i]);  //muestro el valor en la posicion i del arreglo de tamaño "digits"
		data/=10;                                  //divido al dato por 10 y lo guardo en la misma variable para quedarme sin la parte decimal
	}
}


void BcdToLcd(uint8_t dig_Bcd, gpioConf_t *config){  //dig_Bcd=0b0000
	uint8_t i;
	for(i=0; i<4; i++)
	{
		if(dig_Bcd & 1){
			printf("Se pone en 1 el puerto:%d pin:%d \n",config[i].port,config[i].pin);
		}
		else{
			printf("Se pone en 0 el puerto:%d pin:%d \n",config[i].port,config[i].pin);
		}
	dig_Bcd=dig_Bcd>>1;

	}

}

int main(void)
{
	uint32_t dato=12345;  //Dato en decimal, a convertir de binario a bcd
	uint8_t num_Digi[5];     //arreglo de digitos bcd a cargar
	BinaryToBcd(dato, 5, num_Digi); //llamada a funcion para obtener vector cargado


	gpioConf_t configuracion[]={{PUERTO1,PIN4,SALIDA},{PUERTO1,PIN5,SALIDA},{PUERTO1,PIN6,SALIDA},{PUERTO2,PIN14,SALIDA}};

	uint8_t i;
	for (i=0; i<5; i++)
		{	printf("NUMERO DECIMAL a mostrar en el display:%d \n",num_Digi[i]);
			BcdToLcd(num_Digi[i], configuracion);
		}

	return 0;
}

/*==================[end of file]============================================*/


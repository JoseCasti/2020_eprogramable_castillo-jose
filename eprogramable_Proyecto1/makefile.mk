########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Ejericio 1: 32 bits con corrimiento
#PROYECTO_ACTIVO = Ejercicio1
#NOMBRE_EJECUTABLE = Ejercicio1.exe

####Ejericio 2: 16 bits con corrimiento
#PROYECTO_ACTIVO = Ejercicio2
#NOMBRE_EJECUTABLE = Ejercicio2.exe

####Ejericio 3: 16 bits mascara
#PROYECTO_ACTIVO = Ejercicio3
#NOMBRE_EJECUTABLE = Ejercicio3.exe

####Ejericio 4: 16 bits mascara
#PROYECTO_ACTIVO = Ejercicio4
#NOMBRE_EJECUTABLE = Ejercicio4.exe

####Ejericio 7: variable32bits
#PROYECTO_ACTIVO = Ejercicio7
#NOMBRE_EJECUTABLE = Ejercicio7.exe 

####Ejericio 9: deteccion bit
#PROYECTO_ACTIVO = Ejercicio9
#NOMBRE_EJECUTABLE = Ejercicio9.exe 

####Ejericio 12: puntero
#PROYECTO_ACTIVO = Ejercicio12
#NOMBRE_EJECUTABLE = Ejercicio12.exe 

####Ejericio 14: struct
#PROYECTO_ACTIVO = Ejercicio14
#NOMBRE_EJECUTABLE = Ejercicio14.exe 


####Ejericio 16: desplazamiento y union
#PROYECTO_ACTIVO = Ejercicio16
#NOMBRE_EJECUTABLE = Ejercicio16.exe 

####Ejericio 17: Promediador 
#PROYECTO_ACTIVO = Ejercicio17
#NOMBRE_EJECUTABLE = Ejercicio17.exe 

####Ejericio A: Funcion que recibe un puntero a estructura 
#PROYECTO_ACTIVO = EjercicioA
#NOMBRE_EJECUTABLE = EjercicioA.exe 

####Ejericio C: Funcion 
#PROYECTO_ACTIVO = EjercicioC
#NOMBRE_EJECUTABLE = EjercicioC.exe 

####Ejericio D: Funcionn 
PROYECTO_ACTIVO = EjercicioD
NOMBRE_EJECUTABLE = EjercicioD.exe 
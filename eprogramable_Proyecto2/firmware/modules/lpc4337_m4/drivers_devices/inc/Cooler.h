/* Copyright 2020
 * Jose Castillo
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef COOLER_H
#define COOLER_H

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Cooler
 ** @{ */


/*
 * Initials     Name
 * ---------------------------
 *  JC			Jose Castillo
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20201104 v0.1 JC initial version
 *
 */

/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"
/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @brief Función de inicialización del pin para el controlador del cooler a conectar en la placa EDU-CIAA
 * @param[in] pin gpio a utilizar
 */
void CoolerInit(gpio_t dout);

/** @brief Función para poner a 1 el pin elegido de la EDU-CIAA
 * La funion pone en estado alto el pin para el controlador del Cooler
 */
void CoolerOn(void);


/** @brief Función para apagar el pin elegido de la EDU-CIAA
 * 	La funion pone en estado bajo el pin para el controlador del Cooler
 */
void CoolerOff(void);


/** @brief Funcion de deinicializacion del pin para el controlador del cooler conectado a la placa EDU-CIAA
 *	Deinicializa el pin determinado en la placa EDU-CIAA
 *
 */
void CoolerDeinit(gpio_t dout);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef COOLER_H */


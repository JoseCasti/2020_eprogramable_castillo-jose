/*! @mainpage Contador de objetos en cinta transportadora
 *
 * \section genDesc General Description
 *
 * 	Muestra la cantidad de objetos contados utilizando los leds, como un contador binario 4 bits.
 *	El led RGB (azul) representa el bit 3, LED_1 el bit 2,
 *	LED_2 el bit 1 y LED_3 el bit 0.
 *
 *	Funciones de los pulsadores:
 *	TEC1 para activar y detener el conteo.
 *	TEC2 para mantener el resultado (“HOLD”).
 *	TEC3 para resetear el conteo.
 *
 *
 * <a href="https://drive.google.com/file/d/14W1yIfcbYNJ3hOK8QchQEg5sXU1p9TEm/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/09/2020 | Document creation		                         |
 * | 21/09/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _CONTADOR_PROYECTO2_H
#define _CONTADOR_PROYECTO2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_PROYECTO2_H */


﻿Contador_Proyecto2
  
 PROYECTO 2 FINAL 
 
 Muestra la cantidad de objetos contados utilizando los leds, como un contador binario 4 bits.
 El led RGB (azul) representa el bit 3, el LED_1 el bit 2, el LED_2 el bit 1 y el LED_3 el bit 0.
 
 Funciones de los pulsadores:
 TEC1 para activar y detener el conteo.
 TEC2 para mantener el resultado (“HOLD”).
 TEC3 para resetear el conteo.
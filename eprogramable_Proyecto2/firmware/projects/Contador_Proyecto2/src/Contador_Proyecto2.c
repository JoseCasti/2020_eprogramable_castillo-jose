/*! @mainpage Contador de objetos en cinta transportadora
 *
 * \section genDesc General Description
 *
 * 	Muestra la cantidad de objetos contados, utilizando los leds como un contador binario de 4 bits.
 *	El led RGB (azul) representa el bit 3, LED_1 el bit 2,
 *	LED_2 el bit 1 y LED_3 el bit 0.
 *
 *	Funciones de los pulsadores:
 *	TEC1 para activar y detener el conteo.
 *	TEC2 para mantener el resultado (“HOLD”).
 *	TEC3 para resetear el conteo.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/09/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Contador_Proyecto2.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"


/*==================[macros and definitions]=================================*/

#define Desp_BIT0 7		/* Para el desplazamiento del bit0 7 posiciones	*/
#define Desp_BIT1 6		/* Para el desplazamiento del bit1 6 posiciones	*/
#define Desp_BIT2 5		/* Para el desplazamiento del bit2 5 posiciones	*/
#define Desp_BIT3 4		/* Para el desplazamiento del bit3 4 posiciones	*/
#define verificador 0x80  /*Valor 0b10000000 utilizado para comparar con las mascaras de cada bit */

/*==================[internal data definition]===============================*/

uint8_t contador=0,activo=0,hold=0;

/*==================[internal functions declaration]=========================*/

void func_Tecla1(void){
	activo=!activo; 	/* Si se pulsa el sw1 'activo' se inverte de estado y contador se reinicia */
	contador=0;
}

void func_Tecla2(void){
hold=!hold;		/* Si se pulsa el sw2 se invierte el estado de hold */
}

void func_Tecla3(void){
	contador=0;  /* Reinicio del contador al pulsar sw3 */

}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	bool estado_actual,estado_anterior=false;
	uint8_t mascara;
	/* LLamada a funciones de inicializacion */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	/* Se llama a las funciones que permiten interrupciones */
	SwitchActivInt(SWITCH_1, func_Tecla1);
	SwitchActivInt(SWITCH_2, func_Tecla2);
	SwitchActivInt(SWITCH_3, func_Tecla3);


	while(1)
	{

	if(activo)
		{

		estado_actual=Tcrt5000State();  /* Asigna el estado sensado true o false, al estado_actual */

		if(	(estado_actual==1)&&(estado_anterior==0))
			{contador++;}


		if(!hold)
			{
			LedOff(LED_3);
			LedOff(LED_2);
			LedOff(LED_1);
			LedOff(LED_RGB_B);

			/* Encendido de leds mediante manejo de mascaras */
			mascara &= contador<<Desp_BIT0;
			if(mascara == verificador)
				{	LedOn(LED_3); }
			mascara = verificador;

			mascara &= contador<<Desp_BIT1;
			if(mascara == verificador)
				{	LedOn(LED_2); }
			mascara = verificador;

			mascara &=contador<<Desp_BIT2;
			if(mascara == verificador)
				{	LedOn(LED_1); }
			mascara = verificador;

			mascara &= contador<<Desp_BIT3;
			if(mascara == verificador)
				{	LedOn(LED_RGB_B); }
			mascara = verificador;
			}
		}
	estado_anterior=estado_actual;
	}

	Tcrt5000Deinit(GPIO_T_COL0);
}

/*==================[end of file]============================================*/


/*! @mainpage Contador de objetos en cinta transportadora
 *
 * \section genDesc General Description
 *
 * 	Muestra la cantidad de objetos contados utilizando los leds, como un contador binario 4 bits.
 *	El led RGB (azul) representa el bit 3, LED_1 el bit 2,
 *	LED_2 el bit 1 y LED_3 el bit 0.
 *
 *	Muestra la cantidad de objetos contados utilizando la pantalla de la PC. Y las teclas "O", "H" y "0"
 *	para tambien realizar las funciones de los pulsadores.
 *
 *	Funciones de los pulsadores:
 *	TEC1 para activar y detener el conteo.
 *	TEC2 para mantener el resultado (“HOLD”).
 *	TEC3 para resetear el conteo.
 *
 *	Funciones del teclado de la PC:
 *	"O" para activar y detener el conteo.
 *	"H" para mantener el resultado (“HOLD”).
 *	"0" para resetear el conteo.
 *
 *
 * <a href="https://drive.google.com/file/d/1qwhopo3F4pqGW0hibedoMUGZCfV007zd/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 7/10/2020 | Document creation		                         |
 * | 7/10/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _CONTADOR_PROYECTO3_H
#define _CONTADOR_PROYECTO3_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_PROYECTO3_H */


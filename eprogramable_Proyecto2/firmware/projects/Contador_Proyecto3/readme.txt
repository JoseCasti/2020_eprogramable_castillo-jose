﻿Contador_Proyecto3
  
 PROYECTO 3
 
 Muestra la cantidad de objetos contados utilizando los leds, como un contador binario 4 bits. 
 El led RGB (azul) representa el bit 3, el LED_1 el bit 2, el LED_2 el bit 1 y el LED_3 el bit 0.
 A su vez muestra la cantidad de objetos contados utilizando la pantalla de la PC. y las teclas "O", "H" y "0"
 para realizar las funciones de los pulsadores.   
 
 Funciones de los pulsadores:
 TEC1 para activar y detener el conteo.
 TEC2 para mantener el resultado (“HOLD”).
 TEC3 para resetear el conteo.
 
 Funciones del teclado de la PC:
 "O" para activar y detener el conteo.
 "H" para mantener el resultado (“HOLD”).
 "0" para resetear el conteo.
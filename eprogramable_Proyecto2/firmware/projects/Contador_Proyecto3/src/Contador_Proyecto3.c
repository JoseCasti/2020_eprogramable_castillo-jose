/*! @mainpage Contador de objetos en cinta transportadora
 *
 * \section genDesc General Description
 *
 * 	Muestra la cantidad de objetos contados utilizando los leds, como un contador binario 4 bits.
 *	El led RGB (azul) representa el bit 3, LED_1 el bit 2,
 *	LED_2 el bit 1 y LED_3 el bit 0.
 *
 *	Muestra la cantidad de objetos contados utilizando la pantalla de la PC. Y las teclas "O", "H" y "0"
 *	para tambien realizar las funciones de los pulsadores.
 *
 *	Funciones de los pulsadores:
 *	TEC1 para activar y detener el conteo.
 *	TEC2 para mantener el resultado (“HOLD”).
 *	TEC3 para resetear el conteo.
 *
 *	Funciones del teclado de la PC:
 *	"O" para activar y detener el conteo.
 *	"H" para mantener el resultado (“HOLD”).
 *	"0" para resetear el conteo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 7/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Contador_Proyecto3.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

#define Desp_BIT0 7		/* Para el desplazamiento del bit0 7 posiciones	*/
#define Desp_BIT1 6		/* Para el desplazamiento del bit1 6 posiciones	*/
#define Desp_BIT2 5		/* Para el desplazamiento del bit2 5 posiciones	*/
#define Desp_BIT3 4		/* Para el desplazamiento del bit3 4 posiciones	*/
#define verificador 0x80  /*Valor 0b10000000 utilizado para comparar con las mascaras de cada bit */

/*==================[internal data definition]===============================*/

uint8_t mascara,contador=0,activo=0,hold=0;


/*==================[internal functions declaration]=========================*/

/** @brief Función para mostrar el conteo mediante los leds de la placa EDU-CIAA
 *
 *  La funcion apaga todos los leds y los enciende dependiendo del valor contado
 *
 */
void mostrar_Leds(void);



/** @brief Función para activar y detener el conteo
 *
 * 	La funcion permite al pulsar el TEC1, la variable 'activo' se inverte de estado y contador se reinicia
 *
 */
void func_Tecla1(void);


/** @brief Función para mantener el resultado en los leds de la placa EDU-CIAA
 *
 *  La funcion invierte el estado de 'hold' al pulsar el TEC2.
 *  El conteo continua al pulsar TEC2
 *
 */
void func_Tecla2(void);


/** @brief Función para resetear el conteo
 *
 *  La funcion reinicia el contador al pulsar TEC3
 *
 */
void func_Tecla3(void);



/** @brief Función de actualizacion de la muestra del conteo
 *
 * Permite realizar el muestreo según el periodo establecido en el temporizador, que llama la funcion
 * El muestreo del conteo se realiza mediante los leds de la placa EDU-CIAA y la pantalla de la PC
 *
 */
void actualizar_Muestra(void);


/** @brief Función de lectura de datos ingresados por teclado de PC
 *
 * Permite replicar las funciones establecidas en los pulsadores de la placa EDU-CIAA.
 * Lee la opcion elegida por teclado de PC y según ésta llama la funcion del pulsador correspondiente
 *
 */
void lectura_Uart(void);







void func_Tecla1(void){
	activo=!activo; 	/* Si se pulsa el sw1 'activo' se inverte de estado y contador se reinicia */
	contador=0;
}

void func_Tecla2(void){
hold=!hold;		/* Si se pulsa el sw2 se invierte el estado de hold */
}

void func_Tecla3(void){
	contador=0;  /* Reinicio del contador al pulsar sw3 */
}

void mostrar_Leds(void){
	LedsOffAll(); /*Apaga todos los leds*/

	/* Encendido de leds mediante manejo de mascaras */
	mascara &= contador<<Desp_BIT0;
	if(mascara == verificador)
		{	LedOn(LED_3); }
	mascara = verificador;
	mascara &= contador<<Desp_BIT1;
	if(mascara == verificador)
		{	LedOn(LED_2); }
	mascara = verificador;

	mascara &=contador<<Desp_BIT2;
	if(mascara == verificador)
		{	LedOn(LED_1); }
	mascara = verificador;

	mascara &= contador<<Desp_BIT3;
	if(mascara == verificador)
		{	LedOn(LED_RGB_B); }
	mascara = verificador;
}

void actualizar_Muestra(void){  /* Funcion que se llama segun el periodo determinado */
	if(!hold)
		{
		mostrar_Leds();
		UartSendString(SERIAL_PORT_PC, UartItoa(contador, 10) ); /* Funcion para mostrar en pantalla el conteo */
		UartSendString(SERIAL_PORT_PC, " lineas\r\n" );
		}
}

void lectura_Uart(void){ /* Funcion que se va llamar cada vez que se ingresen datos por teclado*/
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	switch(dato)
	    	{
	    		case 'O':
	    			func_Tecla1();
	    		break;
	    		case 'H':
	    			func_Tecla2();
	    		break;
	    		case '0':
	    			func_Tecla3();
	    		break;
	    	}
}





/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	bool estado_actual,estado_anterior=false;

	timer_config my_timer={TIMER_A,1000,actualizar_Muestra}; /*Elijo el timer, periodo y el puntero de funcion a llamar*/

	serial_config my_uart={SERIAL_PORT_PC,115200,lectura_Uart}; /*Elijo puerto, el num de baudios, y la funcion a llamar*/


	/* LLamada a funciones de inicializacion */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	/* Se llama a las funciones que permiten interrupciones */
	SwitchActivInt(SWITCH_1, func_Tecla1);
	SwitchActivInt(SWITCH_2, func_Tecla2);
	SwitchActivInt(SWITCH_3, func_Tecla3);


	TimerInit(& my_timer);
	TimerStart(TIMER_A); /* Empieza la temporizacion */


	UartInit(&my_uart); /*Inicializacion del puerto*/

	while(1)
	{

	if(activo)
		{

		estado_actual=Tcrt5000State();  /* Asigna el estado sensado true o false, al estado_actual */

		if(	(estado_actual==1)&&(estado_anterior==0))
			{contador++;
			}

	estado_anterior=estado_actual;
		}

	}
	TimerStop(TIMER_A);
	Tcrt5000Deinit(GPIO_T_COL0);
}

/*==================[end of file]============================================*/


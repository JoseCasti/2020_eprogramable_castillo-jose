/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "EjContadorLineas1C2018.h"
#include "systemclock.h"
#include "analog_io.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"
#include "buzzer.h"
#include "Tcrt5000.h"


/*==================[macros and definitions]=================================*/

uint16_t contador=0;
bool estado_actual,estado_anterior=FALSE;

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void alertas(void){

	if(contador<50)
	{LedOn(LED_RGB_G);}

	if(contador>50 && contador<150)
	{LedOn(LED_RGB_B);}

	if(contador>150)
	{BuzzerOn();
	 LedOn(LED_RGB_R);}

	LedsOffAll();
	BuzzerOff();

}





void mostrar_Contador(void){  /* Funcion que se llama segun el periodo determinado */

		alertas();
		UartSendString(SERIAL_PORT_PC, UartItoa(contador, 10) ); /* Funcion para mostrar en pantalla el conteo */
		UartSendString(SERIAL_PORT_PC, " lineas\r\n" );

}


void init_Sistema(void){

		timer_config timer={TIMER_A,1000,mostrar_Contador};
		serial_config uart={SERIAL_PORT_PC,9600,NULL};

	/* LLamada a funciones de inicializacion */
		SystemClockInit();
		LedsInit();
		Tcrt5000Init(GPIO_T_COL0);
		BuzzerInit();
		TimerInit(&timer);
		UartInit(&uart); /*Inicializacion del puerto*/
		TimerStart(TIMER_A); /* Empieza la temporizacion */

}



int main(void)
{
	init_Sistema();

	while(1)
		{

		estado_actual=Tcrt5000State();
		if(	(estado_actual==1)&&(estado_anterior==0) )
			{contador++;}
		if(contador>200)
		 	{contador=0;}

		}


	TimerStop(TIMER_A);
	BuzzerDeinit();
	Tcrt5000Deinit(GPIO_T_COL0);
	return 0;
}

/*==================[end of file]============================================*/


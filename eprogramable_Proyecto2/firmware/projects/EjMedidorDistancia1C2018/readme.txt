PARCIAL PRESION ARTERIAL

En el firmware se inicializó el gpio7, y se utilizó las interrupciones destinadas a este. Siendo que una se activa por flanco descendente y otra por ascendente.
Estas interrupciones prenden o apagan una bandera, la cual habilitará (o no) el AnalogInputRead.

Se inicializa la adquisicion analógica y se utiliza el timer systick con una frecuencia de 100 Hz (cada 10 ms).

Se utiliza un contador decremental que parte de 100 (para las 100 muestras), que disminuye por cada activacion del systick

Cada vez que el contador llegue a cero, va a representar 1 segundo de conteo, y se muestran los datos.

Se utiliza interrupcion del serial para iniciar el proceso o detenerlo, modificando el estado del timer (start - stop).
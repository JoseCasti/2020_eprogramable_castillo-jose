/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "EjMedidorDistancia1C2018.h"
#include "systemclock.h"
#include "analog_io.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"
#include "buzzer.h"
#include "hc_sr4.h"



/*==================[macros and definitions]=================================*/
/** @def TEMPMAX
*	@brief Temperatura maxima en °C, que se permite antes de encender el cooler
*/
#define UN_METRO 100


/** @def TEMPMAX
*	@brief Temperatura maxima en °C, que se permite antes de encender el cooler
*/
#define DOS_METROS 200




int16_t distancia; 	/**< Variable para guardar el valor medido de distancia en cm */

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/



void alertas(void){
	if(distancia < UN_METRO){
		LedOn(LED_RGB_R);
		BuzzerOn();
	}

	if(distancia > DOS_METROS){
		LedOn(LED_RGB_G);
	}



	if(distancia >= UN_METRO && distancia <= DOS_METROS){
		LedOn(LED_RGB_B);
	}

	if(distancia < DOS_METROS){
		UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10) ); /* Funcion para mostrar en pantalla el conteo */
		UartSendString(SERIAL_PORT_PC, " cm\r\n" );
	}

}





void medir_Distancia(void){  /* Funcion que se llama segun el periodo determinado */

	distancia=HcSr04ReadDistanceCentimeters();
	BuzzerOff();
	LedsOffAll();
	alertas();
}



void init_Sistema(void){

		timer_config timer={TIMER_A,1000,medir_Distancia}; /* Medir distancia cada 1 segundo */
		serial_config uart={SERIAL_PORT_PC,9600,NULL};

	/* LLamada a funciones de inicializacion */
		SystemClockInit();
		LedsInit();
		HcSr04Init(GPIO_T_FIL0, GPIO_T_FIL3);
		BuzzerInit();
		TimerInit(&timer);
		UartInit(&uart); /*Inicializacion del puerto*/
		TimerStart(TIMER_A); /* Empieza la temporizacion */
		BuzzerSetFrec(5); /* Frecuencia 5Hz, periodo 200ms */

}



int main(void)
{
	init_Sistema(); /* Funcion de inicializacion del sistema */

	while(1)
		{


		}

	BuzzerDeinit();
	HcSr04Deinit(GPIO_T_FIL0, GPIO_T_FIL3);
	return 0;
}

/*==================[end of file]============================================*/


/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../EjOdometro2C2020/inc/EjOdometro2C2020.h"

#include "systemclock.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"
#include "Tcrt5000.h"
#include "switch.h"




/*==================[macros and definitions]=================================*/
/** @def LONG_ABERTURA
*	@brief Longitud de cada abertura y espacio entre aberturas
*/
#define LONG_ABERTURA 2.5







float distancia=0.0;		/**< Variable para guardar el valor de la distancia */
bool encendido=FALSE;		/**< Variable para encender o apagar. Se inicializa el dispositivo apagado */
bool estado_actual=FALSE;	/**< Variable para guardar el estado actual del sensor */
bool estado_anterior=FALSE;	/**< Variable para guardar el estado anterior del sensor */



/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/




void func_Tecla1(void){
	encendido=TRUE;
}

void func_Tecla2(void){
	encendido=FALSE;
}

void func_Tecla3(void){
	distancia=0.0;
}





void enviar_Distancia(void){
	if(encendido){
	UartSendByte(SERIAL_PORT_PC,&distancia);
    UartSendString(SERIAL_PORT_PC, " cm\r\n");
	}
}







void init_Sistema(void){

		timer_config timer={TIMER_A,1000,enviar_Distancia}; /* frecuencia de muestreo  */
		serial_config uart={SERIAL_PORT_PC,57600,NULL};


	/* LLamada a funciones de inicializacion */
		SystemClockInit();
		LedsInit();
		UartInit(&uart); 						/*Inicializacion de la UART*/
		Tcrt5000Init(GPIO_T_COL0);
		SwitchesInit();
		SwitchActivInt(SWITCH_1, func_Tecla1);	/* LLama a la funcion, para encender el dispositivo */
		SwitchActivInt(SWITCH_2, func_Tecla2);	/* LLama a la funcion, para apagar el dispositivo */
		SwitchActivInt(SWITCH_3, func_Tecla3);	/* LLama a la funcion, para resetear el medidor */
		TimerInit(&timer);						/* Se inicializa el timer */
		TimerStart(TIMER_A);					/* Se activa el timer */


}



int main(void)
{
	init_Sistema(); /* Funcion de inicializacion del sistema */

	while(1)
		{


	estado_actual=Tcrt5000State();
	if(estado_actual == TRUE && estado_anterior == FALSE)
		{distancia= distancia + LONG_ABERTURA;}

	estado_anterior=estado_actual;

		}

	Tcrt5000Deinit(GPIO_T_COL0);
	TimerStop(TIMER_A);
	return 0;
}

/*==================[end of file]============================================*/


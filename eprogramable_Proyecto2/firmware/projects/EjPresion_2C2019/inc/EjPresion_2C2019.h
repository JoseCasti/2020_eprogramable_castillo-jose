/*! @mainpage Osciloscopio Digital
 *
 * \section genDesc General Description
 *
  *Digitaliza la senial de un sensor de presion conectado al CH1 de la EDU-CIAA, tomando muestras a una frecuencia de 250Hz.
 *El sensor se comporta de manera lineal: 0mmHg -> 0V y 200mHg -> 3.3V
 *Las teclas 1, 2 y 3 de la EDU-CIAA permiten obtener los valores maximos, minimos y promedio por segundo.
 *Cada tecla seleccionada tiene su LED indicador que enciende cada vez que se selecciona.
 *Estos valores se muestran en el display LCD y en la PC mediante la UART.
 *
 *Para el valor maximo de presion:
 *Si el valor maximo supera 150mmHg se enciende el led RGB rojo
 *Si el valor maximo es mayor a 50mmHg y menor a 150mmHg se enciende el led RGB azul
 *Si el valor maximo es menor a 50mmHg se enciende el led RGB verde
 *
 *Funciones de los pulsadores y LEDs:
 *La tecla 1 para obtener maximo. Enciende led rojo
 *La tecla 2 para obtener minimo. Enciende led amarillo
 *La tecla 3 para obtener promedio. Enciende led verde
 *
 *
 * <a href="https://drive.google.com/file/d/1Mu9Y6CWdcTJmC4QSoJB7RDXLsTgUjLHv/view?usp=sharing">Operation Example</a>
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  	SENSOR 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  OUT		|     CH1	    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |   2/3/2021 | Document creation		                         |
 * |   2/3/2021	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _EJPRESION_2C2019_H
#define _EJPRESION_2C2019_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _OSCDIG_PROYECTO4_H */


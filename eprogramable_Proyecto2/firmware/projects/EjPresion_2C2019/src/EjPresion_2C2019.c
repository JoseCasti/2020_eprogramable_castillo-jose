/*! @mainpage Adquisicion presion arterial
 *
 * \section genDesc General Description
 *
 *Digitaliza la senial de un sensor de presion conectado al CH1 de la EDU-CIAA, tomando muestras a una frecuencia de 250Hz.
 *El sensor se comporta de manera lineal: 0mmHg -> 0V y 200mHg -> 3.3V
 *Las teclas 1, 2 y 3 de la EDU-CIAA permiten obtener los valores maximos, minimos y promedio por segundo.
 *Cada tecla seleccionada tiene su LED indicador que enciende cada vez que se selecciona.
 *Estos valores se muestran en el display LCD y en la PC mediante la UART.
 *
 *Para el valor maximo de presion:
 *Si el valor maximo supera 150mmHg se enciende el led RGB rojo
 *Si el valor maximo es mayor a 50mmHg y menor a 150mmHg se enciende el led RGB azul
 *Si el valor maximo es menor a 50mmHg se enciende el led RGB verde
 *
 *Funciones de los pulsadores y LEDs:
 *La tecla 1 para obtener maximo. Enciende led rojo
 *La tecla 2 para obtener minimo. Enciende led amarillo
 *La tecla 3 para obtener promedio. Enciende led verde
 *
 *
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  	SENSOR 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  OUT		|     CH1	    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |  2/3/2021  | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "EjPresion_2C2019.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "gpio.h"
#include "lcditse0803.h"

/*==================[macros and definitions]=================================*/
/** @def BAUD_RATE
*	@brief Velocidad de transmision de datos en bits/segundo
*/
#define BAUD_RATE 9600



/** @def FREC_MUESTREO
*	@brief Periodo de muestreo 4ms. Ya que frecuencia de muestreo 250Hz
*/
#define PERIODO 4



/** @def PA_MAX
*	@brief Presion arterial maxima que se puede tener en la entrada del conversor AD
*/
#define PA_MAX 200



/** @def PA_MIN
*	@brief Presion arterial minima que se puede tener en la entrada del conversor AD
*/
#define PA_MIN 0







/*==================[internal data definition]===============================*/

uint8_t contador=0;		/**< Variable para realizar el conteo de muestras obtenidas */
float valor;			/**< Variable para guardar el valor actual medido de la presion */
float PA_Max=PA_MIN;	/**< Variable para guardar el valor de presion maximo medido en el ultimo segundo */
float PA_Min=PA_MAX;	/**< Variable para guardar el valor de presion minimo medido en el ultimo segundo */
float PA_Prom;				/**< Variable para guardar el valor de presion promedio medido en el ultimo segundo */
float acumulador=PA_MIN;	/**< Variable para guardar la suma de los valores de presion medidos en el ultimo segundo */






/*==================[internal functions declaration]=========================*/

/** @brief Función para mostrar el valor maximo de presion
 *
 * 	La funcion permite al pulsar TEC1, mostrar el valor de presion maximo, sensado en
 * 	el ultimo segundo, al display LCD y a la PC mediante la UART
 *
 */
void send_Max(void);

/** @brief Función para enviar el valor minimo de presion
 *
 * 	La funcion permite al pulsar TEC2, mostrar el valor de presion minimo, sensado en
 * 	el ultimo segundo, al display LCD y a la PC mediante la UART
 *
 */
void send_Min(void);


/** @brief Función para enviar el valor promedio de presion
 *
 * 	La funcion permite al pulsar TEC3, mostrar el valor de presion promedio, sensado en
 * 	el ultimo segundo, al display LCD y a la PC mediante la UART
 *
 */
void send_Prom(void);



/** @brief Función para realizar la conversion
 *
 * 	La funcion llama a la funcion correspondiente para la conversion AD
 * 	y es solicitada cada 4ms por el timer.
 *
 */
void convert(void);



/** @brief Función para la inicializacion de los modulos del sistema
 *
 * 	La funcion llama a las funciones de inicializacion correspondientes
 *
 */
void init_System(void);












	/* Implementaciones de funciones */


void send_Max(void){
	LedsOffAll();
	LedOn(LED_1);
	UartSendString(SERIAL_PORT_PC, UartItoa(PA_Max, 10) );  /* Envia el valor max de la presion en 1 seg a la PC */
	UartSendString(SERIAL_PORT_PC," mmHg\n");
	LcdItsE0803Write(PA_Max);

	/* Indicadores Leds para el rango que se encuentra la presion maxima */
	if(PA_Max<50){
		LedOn(LED_RGB_G);
	}

	if(PA_Max>=50 && PA_Max<=150){
		LedOn(LED_RGB_B);
	}

	if(PA_Max>150){
		LedOn(LED_RGB_R);
	}
}



void send_Min(void){
	LedsOffAll();
	LedOn(LED_2);
	UartSendString(SERIAL_PORT_PC, UartItoa(PA_Min, 10) );  /* Envia el valor min de la presion en 1 seg a la PC */
	UartSendString(SERIAL_PORT_PC," mmHg\n");
	LcdItsE0803Write(PA_Min);
}



void send_Prom(void){
	LedsOffAll();
	LedOn(LED_3);
	UartSendString(SERIAL_PORT_PC, UartItoa(PA_Prom, 10) );  /* Envia el valor promedio de la presion en 1 seg a la PC */
	UartSendString(SERIAL_PORT_PC," mmHg\n");
	LcdItsE0803Write(PA_Prom);
}



void convert(void){  					/* Funcion para la conversion AD cada 4ms */
	AnalogStartConvertion(); 			/* Iniciar la conversion */
}



void read_Signal(void){   				/* Interrupcion por dato convertido  */
	AnalogInputRead(CH1, &valor); 		/* Lectura del canal 1, en variable 'valor' */
	contador++;							/* Por cada valor leído aumenta el contador de muestras */
}




void init_System(void){   												/* Inicializacion del sistema */

	timer_config timerADC={TIMER_A,PERIODO,convert};  					/* 4ms por la frecuencia de muestreo de 250Hz */

	analog_input_config presion={CH1,AINPUTS_SINGLE_READ,read_Signal};  /* Lectura de una muestra por vez en canal 1 */

	serial_config uart={SERIAL_PORT_PC,BAUD_RATE,NULL};					/* Mediante UART solo envio de datos a PC */


	/*Inicializaciones*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	AnalogInputInit(&presion); 				/* Inicializacion del ADC, se pasa la direccion de la struct */
	UartInit(&uart); 						/* Inicializacion del puerto */
	LcdItsE0803Init();						/* Inicializacion del display LCD */
	TimerInit(&timerADC);
	TimerStart(timerADC.timer);				/* Se activa el timer */

	SwitchActivInt(SWITCH_1, send_Max); 	/* Envia valor maximo */
	SwitchActivInt(SWITCH_2, send_Min); 	/* Envia valor minimo */
	SwitchActivInt(SWITCH_3, send_Prom); 	/* Envia valor promedio */


}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	init_System();


	while(1)
	{


	if(contador<100){
		acumulador= acumulador + valor;

		if(valor>PA_Max){
			PA_Max=valor;
		}
		if(valor<PA_Min){
			PA_Min=valor;
		}

	}

	else{


	acumulador= acumulador/100.0;			//Calculo de PA promedio

	//Calculo de PA en mmHG
	PA_Prom= ( acumulador * 3.3 )/1024.0;	//para poder usar los V y convertirlos en mmHG
	PA_Prom= PA_Prom * 60.6; 				//PAmax en V a mmHG


	PA_Max= ( PA_Max * 3.3 )/1024.0;		//para poder usar los V y convertirlos en mmHG
	PA_Max= PA_Max * 60.6; 					//PAmax en V a mmHG


	PA_Min= ( PA_Min * 3.3 )/1024.0;		//para poder usar los V y convertirlos en mmHG
	PA_Min= PA_Min * 60.6; 					//PAmax en V a mmHG

	contador=0;								//Se reinicia el contador para otras 100 muestras por segundo
	acumulador=0.0;							//Se reinicia el acumulador de valores para otras 100 muestras por segundo


		}

	}


	LcdItsE0803DeInit();

}

/*==================[end of file]============================================*/


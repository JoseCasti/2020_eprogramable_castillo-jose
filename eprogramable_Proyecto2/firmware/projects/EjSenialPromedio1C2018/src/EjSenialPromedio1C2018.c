/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/EjSenialPromedio1C2018.h"
#include "systemclock.h"
#include "analog_io.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"



/*==================[macros and definitions]=================================*/





int16_t distancia; 	/**< Variable para guardar el valor medido de distancia en cm */
float promedio=0.0;
int8_t contador=0;
int8_t valor[100];

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


void calcular_Promedio(void){
	for(uint8_t i=0; i<100; i++)
		{promedio=promedio + valor[i];}
	promedio=promedio/100.0;
}




void enviar_Promedio(void){
	calcular_Promedio();
	UartSendByte(SERIAL_PORT_PC,UartItoa(promedio, 10));
	UartSendString(SERIAL_PORT_PC,"\r\n");
	GPIOToggle(GPIO_1);
}





void leer_Senial(void){
	if(contador==100){
		contador=0;
		TimerStart(TIMER_B); /* Empieza la temporizacion para enviar el promedio/segundo */
	}
	AnalogInputRead(CH1, &valor[contador]);
	LedToggle(LED_RGB_B);
	contador++;

}





void actualizar_Muestra(void){  /* Funcion que se llama segun el periodo determinado */
	AnalogStartConvertion();
}



void init_Sistema(void){

		timer_config timerMuestra={TIMER_A,10,actualizar_Muestra}; /* frecuencia de muestreo 100Hz */
		serial_config uart={SERIAL_PORT_PC,57600,NULL};
		analog_input_config senial_Entrada={CH1,AINPUTS_SINGLE_READ,leer_Senial};

		timer_config timerProm={TIMER_B,1000,enviar_Promedio}; /* frecuencia de muestreo 100Hz */
		AnalogInputInit(&senial_Entrada);

	/* LLamada a funciones de inicializacion */
		SystemClockInit();
		LedsInit();
		TimerInit(&timerMuestra);
		UartInit(&uart); /*Inicializacion del puerto*/
		TimerStart(TIMER_A); /* Empieza la temporizacion */
		GPIOInit(GPIO_1, GPIO_OUTPUT);
		LedOff(LED_RGB_B);
		GPIOOff(GPIO_1);
		TimerInit(&timerProm);


}



int main(void)
{
	init_Sistema(); /* Funcion de inicializacion del sistema */

	while(1)
		{


		}

	GPIODeinit();
	return 0;
}

/*==================[end of file]============================================*/


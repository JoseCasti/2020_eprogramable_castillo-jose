/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "EjVoltajePot2C2020.h"

#include "systemclock.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"
#include "analog_io.h"



/*==================[macros and definitions]=================================*/





uint8_t contador=0;		/**< Variable para contar las 50 muestras por segundo */
float promedio=0.0;		/**< Variable para guardar el valor promedio/segundo */
uint8_t valor[50];		/**< Variable para guardar el valor del voltaje por muestra */




/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/





void calcular_Promedio(void){

	for(uint8_t i=0; i<50; i++)
	{promedio = promedio + valor[i];}

	promedio = promedio/50.0;
}



void actualizar_Muestra(void){
	AnalogStartConvertion();
}


void leer_Senial(void){
	AnalogInputRead(CH2, &valor[contador]);
	if(contador==50){
			contador=0;
			TimerStart(TIMER_B); /* Empieza la temporizacion para enviar el promedio/segundo */
		}
	contador++;
}

void enviar_Promedio(void){
	calcular_Promedio();
	UartSendString(SERIAL_PORT_PC, UartItoa(promedio, 10) );  /* Envia el valor de la temperatura a la PC */
	UartSendString(SERIAL_PORT_PC," V\r\n");
}







void init_Sistema(void){

		timer_config timerMuestra={TIMER_A,20,actualizar_Muestra}; /* frecuencia de muestreo 50Hz */
		serial_config uart={SERIAL_PORT_PC,57600,NULL};
		timer_config timerPromedio={TIMER_B,1000,enviar_Promedio}; /* Para enviar promeido /segundo */
		analog_input_config adc={CH2,AINPUTS_SINGLE_READ,leer_Senial};  /* Lectura de una muestra por vez en canal 1  */

	/* LLamada a funciones de inicializacion */
		SystemClockInit();
		LedsInit();
		UartInit(&uart); /*Inicializacion del puerto*/
		AnalogInputInit(&adc);
		TimerInit(&timerMuestra);
		TimerInit(&timerPromedio);

		TimerStart(TIMER_A);



}



int main(void)
{
	init_Sistema(); /* Funcion de inicializacion del sistema */

	while(1)
		{



		}


	TimerStop(TIMER_A);
	TimerStop(TIMER_B);

	return 0;
}

/*==================[end of file]============================================*/


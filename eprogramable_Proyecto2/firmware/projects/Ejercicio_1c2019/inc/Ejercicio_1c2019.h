/*! @mainpage Osciloscopio Digital
 *
 * \section genDesc General Description
 *
 * 	Muestra mediante el graficador "Serial Oscilloscope" una señal de ECG.
 *	Permite aplicar un filtro digital pasa-bajo sobre la señal de ECG, y
 *	visualizar la señal filtrada en el graficador.
 *	Mediante los pulsadores de la placa EDU-CIAA se controla el filtro digital.
 *
 *	Funciones de los pulsadores:
 *	La tecla 1 para activar el uso del filtro.
 *	La tecla 2 para desactivar el uso del filtro.
 *	La tecla 3 para bajar la frecuencia de corte.
 *	La tecla 4 para subir la frecuencia de corte.
 *
 *
 *
 * <a href="https://drive.google.com/file/d/1Mu9Y6CWdcTJmC4QSoJB7RDXLsTgUjLHv/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN ext		| 	   DAC	    |
 * | 	PIN med	 	| 	   CH1  	|
 * | 	PIN ext	 	| 	   GNDA  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 22/10/2020 | Document creation		                         |
 * | 22/10/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _OSCDIG_PROYECTO4_H
#define _OSCDIG_PROYECTO4_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _OSCDIG_PROYECTO4_H */


﻿OscDig_Proyecto4
  
 PROYECTO 4
 
 Muestra mediante graficador una señal de ECG.Permite aplicar un filtro digital pasa-bajo sobre la señal de ECG, y 
 visualizar la señal filtrada en el graficador.
 Mediante los pulsadores de la placa EDU-CIAA se controla el filtro digital.
 
 Funciones de los pulsadores:
 La tecla 1 para activar el uso del filtro.
 La tecla 2 para desactivar el uso del filtro.
 La tecla 3 para bajar la frecuencia de corte.
 La tecla 4 para subir la frecuencia de corte.
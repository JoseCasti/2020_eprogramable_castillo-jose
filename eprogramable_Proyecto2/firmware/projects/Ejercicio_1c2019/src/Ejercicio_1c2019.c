/*! @mainpage Adquisicion presion arterial
 *
 * \section genDesc General Description
 *
 *
 *
 *
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN ext		| 	   DAC	    |
 * | 	PIN med	 	| 	   CH1  	|
 * | 	PIN ext	 	| 	   GNDA  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 22/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Ejercicio_1c2019.h"       /* <= own header */

#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "gpio.h"
#include "lcditse0803.h"

/*==================[macros and definitions]=================================*/




/*==================[internal data definition]===============================*/

bool iniciar=FALSE;			/**< Variable para activar o desactivar el filtro. Se inicializa el filtro apagado */
bool on=FALSE;			/**< Variable para activar o desactivar el filtro. Se inicializa el filtro apagado */
uint8_t contador=100;			/**< Variable para activar o desactivar el filtro. Se inicializa el filtro apagado */
uint8_t valor;
uint8_t comparador=0;
float PAmax;




/*==================[internal functions declaration]=========================*/










	/* Implementaciones de funciones */


void iniciar_Lectura(void){
	iniciar=TRUE; 	/* Si se pulsa el sw1 'activo' se inverte de estado y contador se reinicia */
}

void fin_Lectura(void){
	iniciar=FALSE;		/* Si se pulsa el sw2 se invierte el estado de hold */
}


void encender(void){
	on=TRUE; 	/* Si se pulsa el sw1 'activo' se inverte de estado y contador se reinicia */
}

void apagar(void){
	on=FALSE;		/* Si se pulsa el sw2 se invierte el estado de hold */
}



void convertir_Senial(void){  				/* Funcion para la conversion ADC cada 10ms */
	AnalogStartConvertion(); 			/* Iniciar la conversion */
}


void leer_Senial(void){   												/* Interrupcion por dato convertido  */
	AnalogInputRead(CH1, &valor); /* Lectura del canal 1 */
	contador++;
}


void control(void){   												/* Interrupcion por dato convertido  */

	if(on == TRUE)

	{

		if(iniciar == TRUE){

			LedOff(LED_RGB_R);
			LedOn(LED_RGB_G);
			TimerStart(TIMER_A);}
	}

	else{
		LedOn(LED_RGB_R);
		LedOff(LED_RGB_G);
		TimerStop(TIMER_A);}

}









/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	timer_config timerADC={TIMER_A,10,convertir_Senial}; /* 10ms por la frecuencia de muestreo de 100Hz */

	analog_input_config presion={CH1,AINPUTS_SINGLE_READ,leer_Senial};  /* Lectura de una muestra por vez en canal 1 */

	serial_config uart={SERIAL_PORT_PC,115200,NULL};		/* Puerto serie PC. Lectura por entrada de PC */


	/*Inicializaciones*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	AnalogInputInit(&presion); 		/* Inicializacion del ADC, se pasa la direccion de la struct */
	UartInit(&uart); 			/* Inicializacion del puerto */
	LcdItsE0803Init();
	TimerInit(&timerADC);

	SwitchActivInt(SWITCH_1, encender); /* Enciende dispositivo e inicia adquisicion */
	SwitchActivInt(SWITCH_2, apagar);		/* Apaga dispositivo */

	GPIOActivInt(GPIOGP5, GPIO_5,iniciar_Lectura, FALSE); /*Inicializacion del pin que indica la adquisicion de datos por flanco descendente */
	GPIOActivInt(GPIOGP5, GPIO_5,fin_Lectura, TRUE); /*Inicializacion del pin que indica la adquisicion de datos por flanco descendente */

	while(1)
	{

	control();
	//logica para que se encuentre el maximo valor en 1 segundo, son 100 muestras/segundo
	if(contador<100){
		if(valor>comparador)
		{comparador=valor;}
	}

	else{

	//calculo de PA maxima Ver escala
	PAmax= ( comparador * 3.3 )/1024.0;		//para poder usar los V y convertirlos en mmHG
	PAmax= ( PAmax + 2.2 ) / 0.044; 		//PAmax en V a mmHG

	//envio de datos a pc y LCD
	UartSendString(SERIAL_PORT_PC, UartItoa(PAmax, 10) );  /* Envia el valor max de la presion en 1 seg a la PC */
	UartSendString(SERIAL_PORT_PC," mmHg\n");
	LcdItsE0803Write(PAmax);

	contador=0;
	comparador=0;}

	}

	LcdItsE0803DeInit();

}

/*==================[end of file]============================================*/


/*! @mainpage Extintor
 *
 * \section genDesc General Description
 *
 *	Este dispositivo permite sensar la temperatura ambiente mediante el CI LM35.
 *	y mostrar dicha temperatura en la PC mediante el software hterm.
 *	Cuando se mide una temperatura mayor a la preestablecida (40°C en este caso) se enciende
 *	el cooler 12V y el led indicador. Este permanece encendio hasta que la temperatura sea
 *	menor al valor establecido
 *
 *	Funciones de los pulsadores:
 *	La tecla 1 para activar el dispositivo.
 *	La tecla 2 para desactivar el dispositivo.
 *
 *	Funciones del teclado de la PC:
 *	"A" para activar  el dispositivo.
 *	"D" para desactivar el dispositivo.
 *
 *
 *
 *
 * <a href="https://drive.google.com/file/d/1UYk9oVDxX8vb5Vausm3t119HPLMHfFJ4/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |  	  LM35      |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  Vout		| 	   CH1	    |
 * | 	  GND	 	| 	   GNDA  	|
 *
 * |  Control Cooler 12V |   EDU-CIAA	|
 * |:-------------------:|:-------------|
 * |    Base BC548C      | 	  GPIO_1    |
 * |    	 GND	   	 | 	   GND  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 31/10/2020 | Document creation		                         |
 * | 5/11/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _EXTINTOR_PROYECTO5_H
#define _EXTINTOR_PROYECTO5_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EXTINTOR_PROYECTO5_H */


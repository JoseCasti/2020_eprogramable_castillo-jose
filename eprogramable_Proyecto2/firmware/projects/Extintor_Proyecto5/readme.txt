﻿Extintor_Proyecto5
 
 PROYECTO 5
 	Este dispositivo permite sensar la temperatura ambiente mediante el CI LM35.
 	y mostrar dicha temperatura en la PC mediante el software hterm.
 	Cuando se mide una temperatura mayor a la preestablecida (40°C en este caso) se enciende
 	el cooler 12V y el led indicador.Este permanece encendio hasta que la temperatura sea 
 	menor al valor establecido
 
 
 	Funciones de los pulsadores:
 	La tecla 1 para activar el dispositivo.
 	La tecla 2 para desactivar el dispositivo.
 
 	Funciones del teclado de la PC:
 	"A" para activar  el dispositivo.
 	"D" para desactivar el dispositivo.
 
	NOTA: 
	Para la prueba de funcionamiento se eligió 40°C para reducir los tiempos de la demostración.
	
	Ademas se queria mostrar la temperatura sensada mediante display de 7 segmentos, pero debido a que
	durante las pruebas del driver max7219 no se mostraba lo indicado, ni se encontró solucion hasta el momento,
	dicho modulo está sin funcionamiento en la aplicacion.
/*! @mainpage Extintor
 *
 * \section genDesc General Description
 *
 *	Este dispositivo permite sensar la temperatura ambiente mediante el CI LM35.
 *	y mostrar dicha temperatura en la PC mediante el software hterm.
 *	Cuando se mide una temperatura mayor a la preestablecida (40°C en este caso) se enciende
 *	el cooler 12V y el led indicador. Este permanece encendio hasta que la temperatura sea
 *	menor al valor establecido
 *
 *	Funciones de los pulsadores:
 *	La tecla 1 para activar el dispositivo.
 *	La tecla 2 para desactivar el dispositivo.
 *
 *	Funciones del teclado de la PC:
 *	"A" para activar  el dispositivo.
 *	"D" para desactivar el dispositivo.
 *
 *
 *
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  	  LM35      |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	  Vout		| 	   CH1	    |
 * | 	  GND	 	| 	   GNDA  	|
 *
 * |  Control Cooler 12V |   EDU-CIAA	|
 * |:-------------------:|:-------------|
 * |    Base BC548C      | 	  GPIO_1    |
 * |    	 GND	   	 | 	   GND  	|
 *
 *
 *
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |  5/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Extintor_Proyecto5.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "spi.h"
#include "cooler.h"
#include "max7219.h"

/*==================[macros and definitions]=================================*/
/** @def TEMPMAX
*	@brief Temperatura maxima en °C, que se permite antes de encender el cooler
*/
#define TEMPMAX 40








/*==================[internal data definition]===============================*/
bool encendido=FALSE;		/**< Variable para encender o apagar. Se inicializa el dispositivo apagado */
uint16_t valor;				/**< Variable para guardar el valor de los Volts */
uint16_t temperatura;		/**< Variable para guardar la temperatura */



/*==================[internal functions declaration]=========================*/

/** @brief Función para encender el dispositivo
 *
 * 	La funcion permite al pulsar el TEC1, la variable 'encendido' es true
 *
 */
void func_Tecla1(void);



/** @brief Función para apagar el dispositivo
 *
 *  La funcion permite al pulsar el TEC2, la variable 'encendido' es false
 *
 */
void func_Tecla2(void);



/** @fn uint16_t obtener_Temp(uint16_t temp)
 * @brief Función para convertir el valor de volts en grados celsius
 * @param[in] Valor digital de la temperatura
 * @return Temperatura en grados celsius
 */
uint16_t obtener_Temp(uint16_t temp);




/** @brief Función para realizar la conversion AD
 *
 *	Llama a la funcion de ADC para la conversion
 *	Se llama la funcion segun el TIMER_A con periodo de 1s
 *
 */
void func_TimerADC(void);




/** @brief	Funcion que se llama por dato convertido en el ADC
 *
 *	LLama a la funcion de ADC que permite leer el canal 1 y guardar el valor digital correspondiente.
 *	LLama a la funcion para convertir el valor digital obtenido
 *
 */
void func_ADC(void);



/** @brief Función de lectura de datos ingresados por teclado de PC
 *
 * Permite replicar las funciones establecidas en los pulsadores de la placa EDU-CIAA.
 * Lee la opcion elegida por teclado de PC y según ésta llama la funcion del pulsador correspondiente
 *
 */
void lectura_Uart(void);











void func_Tecla1(void){
	encendido = TRUE;			/* Activa el dispositivo */
}


void func_Tecla2(void){
	encendido = FALSE;			/* Desactiva el dispositivo */
}



uint16_t obtener_Temp(uint16_t temp){		/* Convierte los V en °C */
	float t;
	t= ( temp * 3.3 )/ 1024.0 ;
	t=t*100.0;								/* Por la salida del sensor 10mV/°C */
	return t;
}


/*void send_Display(uint16_t temp)  //NO ESTA FUNCIONANDO AUN EL DISPLAY
{	//Conversion del entero a char

	MAX7219_DisplayChar(digit, 'A');
}
*/


void func_TimerADC(void){  				/* Funcion para la conversion ADC cada 1 seg */
	AnalogStartConvertion(); 			/* Iniciar la conversion */
}



void func_ADC(void){   					/* Interrupcion por dato convertido  */

	AnalogInputRead(CH1, &valor); 		/* Lectura del canal 1 */
	temperatura=obtener_Temp(valor);
}



void lectura_Uart(void){ /* Funcion que se va llamar cada vez que se ingresen datos por teclado*/
	uint8_t tec;
	UartReadByte(SERIAL_PORT_PC, &tec);
	switch(tec)
	    	{
	    		case 'A':
	    			func_Tecla1();
	    		break;
	    		case 'D':
	    			func_Tecla2();
	    		break;
	    	}
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	timer_config my_timerADC={TIMER_A,1000,func_TimerADC}; 			/* Por la frecuencia de muestreo de 1Hz */
	analog_input_config my_adc={CH1,AINPUTS_SINGLE_READ,func_ADC};  /* Lectura de una muestra por vez en canal 1  */
	serial_config my_uart={SERIAL_PORT_PC,9600,lectura_Uart};		/* Puerto serie Uart. Lectura por entrada de PC */
	//spiConfig_t my_spi={SPI_1,MASTER,MODE0,1000000,SPI_POLLING};	/* Puerto SPI1. Envio de datos a max7219 conectado a la EDU-CIAA */


	/* LLamada a funciones de inicializacion */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(& my_timerADC);		/* Inicializacion del timer */
	AnalogInputInit(&my_adc); 		/* Inicializacion del ADC */
	UartInit(&my_uart); 			/* Inicializacion de la UART */
	CoolerInit(GPIO_1);				/* Inicializacion pin GPIO1 de la EDU-CIAA */
	//SpiInit(my_spi);				/* Inicializacion del puerto SPI1 de la EDU-CIAA */
	//MAX7219_Init();				/* Inicializacion del driver max7219 */

	/* Se llama a las funciones que permiten interrupciones por switches */
	SwitchActivInt(SWITCH_1, func_Tecla1);
	SwitchActivInt(SWITCH_2, func_Tecla2);

	/* Inicio de la temporizacion del timer elegido */
	TimerStart(my_timerADC.timer);



	while(1)
	{


		if(encendido){

			LedOn(LED_RGB_G); 			/* Led de encendido */

			//send_Display(temperatura);

			UartSendString(SERIAL_PORT_PC, UartItoa(temperatura, 10) );  /* Envia el valor de la temperatura a la PC */
			UartSendString(SERIAL_PORT_PC,"C\n");


			if(temperatura>TEMPMAX){
				CoolerOn();					/* Encender cooler */
				LedToggle(LED_1);			/* Led que indica cooler encendido */
			}
			if(temperatura<=TEMPMAX){
				CoolerOff();				/* Apagar cooler */
				LedOff(LED_1);				/* Apagar led que indica cooler encendido */
			}
		}
		else{LedsOffAll();}
	}


	/* LLamada a funciones de deinicializacion */
	CoolerDeinit(GPIO_1);
	TimerStop(my_timerADC.timer);
	//SpiDeInit(my_spi.port);

}


/*==================[end of file]============================================*/


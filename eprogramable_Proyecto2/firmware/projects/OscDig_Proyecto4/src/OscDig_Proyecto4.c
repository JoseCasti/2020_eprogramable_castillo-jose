/*! @mainpage Osciloscopio Digital
 *
 * \section genDesc General Description
 *
 *  Muestra mediante el graficador "Serial Oscilloscope" una señal de ECG.
 *	Permite aplicar un filtro digital pasa-bajo sobre la señal de ECG, y
 *	visualizar la señal filtrada en el graficador.
 *	Mediante los pulsadores de la placa EDU-CIAA se controla el filtro digital.
 *
 *	Funciones de los pulsadores:
 *	La tecla 1 para activar el uso del filtro.
 *	La tecla 2 para desactivar el uso del filtro.
 *	La tecla 3 para bajar la frecuencia de corte.
 *	La tecla 4 para subir la frecuencia de corte.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN ext		| 	   DAC	    |
 * | 	PIN med	 	| 	   CH1  	|
 * | 	PIN ext	 	| 	   GNDA  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 22/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "OscDig_Proyecto4.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
/** @def PERIODO
*	@brief delta de tiempo en segundos entre muestras
*/
#define PERIODO 0.002

/** @def PI
*	@brief Valor aproximado del numero pi
*/
#define PI 3.14

/** @def DELTAF
*	@brief valor de frecuencia elegido para aumentar o bajar la frecuencia de corte
*/
#define DELTAF 10

/** @def BUFFER_SIZE
*	@brief Tamaño del buffer de la señal ECG
*/
#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};


/*==================[internal data definition]===============================*/

bool filtro=FALSE;			/**< Variable para activar o desactivar el filtro. Se inicializa el filtro apagado */
uint16_t valor;				/**< Variable para guardar el valor ya convertido */
uint16_t muestra=0;		   	/**< Variable para recorrer las muestras del ECG */
uint8_t fcorte=150;			/**< Variable para guardar la frecuencia de corte, se inicializa en 150Hz */
uint16_t valor_anterior;	/**< Variable para guardar el ultimo valor filtrado */
uint16_t valor_filtrado;	/**< Variable para guardar el valor filtrado */
float alfa;  				/**< Variable para calcular el valor filtrado, y cambia con la frecuencia de corte */





/*==================[internal functions declaration]=========================*/

/** @brief Funcion para calcular el valor de alfa
 *
 *	La funcion permite calcular la variable alfa y depende de la frecuencia de corte
 *
 */
void calc_Alfa(void);



/** @brief Función para activar el filtro
 *
 * 	La funcion permite al pulsar TEC1 en la placa EDU-CIAA, activar el filtro digital
 *
 */
void func_Tecla1(void);



/** @brief Función para desactivar el filtro
 *
 *  La funcion permite al pulsar TEC2 en la placa EDU-CIAA, desactivar el filtro digital
 *
 */
void func_Tecla2(void);



/** @brief Función para bajar la frecuencia de corte
 *
 *  La funcion permite al pulsar TEC3 en la placa EDU-CIAA, bajar la frecuencia de corte
 *  del filtro digital, en un valor prefijado (10Hz)
 *
 */
void func_Tecla3(void);



/** @brief Función para aumentar la frecuencia de corte
 *
 *  La funcion permite al pulsar TEC4 en la placa EDU-CIAA, aumentar la frecuencia de corte
 *  del filtro digital, en un valor prefijado (10Hz)
 *
 */
void func_Tecla4(void);


/** @brief Función de lectura de teclado
 *
 * Permite replicar las funciones establecidas en los pulsadores de la placa EDU-CIAA.
 * Lee la opcion elegida por teclado de la PC y según ésta llama la funcion del pulsador correspondiente
 *
 */
void leer_Serial(void);



/** @brief Funcion para la escritura de cada muestra en el pin DAC
 *
 *  La funcion recorre cada muestra del buffer ecg y la envia al DAC de la EDU-CIAA
 *  Se llama la funcion segun el TIMER_B con periodo de 4ms
 *
 */
void func_TimerDAC(void);



/** @brief Función para realizar la conversion AD
 *
 *	Llama a la funcion de ADC para la conversion
 *	Se llama la funcion segun el TIMER_A con periodo de 2ms
 *
 */
void func_TimerADC(void);



/** @fn uint16_t filtrar(uint16_t anterior, uint16_t actual)
 * @brief Funcion para filtrar una muestra de la señal de entrada (ECG)
 * @param[in] anterior Ultimo valor filtrado de la muestra
 * @param[in] actual   Valor actual de la muestra sin filtrar
 * @return Entero sin signo. Valor actual filtrado
 */
uint16_t filtrar(uint16_t anterior, uint16_t actual);



/** @brief	Funcion que se llama por dato convertido en el ADC
 *
 *	LLama a la funcion de ADC que permite leer el canal 1 y guardar el valor.
 *	LLama a la funcion filtrar() en caso de que se active el filtro.
 *	Envia los valores filtrados y sin filtrar, por el puerto serie,
 *	mediante la funcion de la UART.
 *
 */
void func_ADC(void);








	/* Implementaciones de funciones */

void calc_Alfa(void){
	alfa= PERIODO / (( 1/(2*PI*fcorte)) + PERIODO);	/* Calculo de alfa con la variable fcorte y la constante PERIODO*/
}



void func_Tecla1(void){
	filtro = TRUE;			/* Activa el uso del filtro */
	calc_Alfa();
}



void func_Tecla2(void){
	filtro = FALSE;			/* Desactiva el uso del filtro */
}



void func_Tecla3(void){
	if(fcorte>10)
	{fcorte = fcorte - DELTAF;	/* Para bajar la frecuencia de corte en DELTAF=10Hz*/
	calc_Alfa();}
}



void func_Tecla4(void){
	if(fcorte<221)
	{fcorte = fcorte + DELTAF;	/* Para subir la frecuencia de corte en DELTAF=10Hz */
	calc_Alfa();}
}



void leer_Serial(void){	/* Esta funcion no se utilizó debido a que no se podían ejecutar hterm y Serial Oscilloscope a la vez*/
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);  /* Para leer la tecla y realizar la funcion correspondiente */
		switch(dato)
		{
			case 'A':
				func_Tecla1();
		 	break;
			case 'D':
				func_Tecla2();
			break;
			case 'S':
				func_Tecla3();
			break;
			case 'B':
				func_Tecla4();
			break;
		}
}



void func_TimerDAC(void){  				/* Funcion para la escritura de cada muestra cada 4ms en el DAC */
	AnalogOutputWrite(ecg[muestra]);	/* Escritura de una muestra del ecg */
	muestra++;							/* Para recorrer las posiciones del vector ecg[231] */
	if(muestra == (BUFFER_SIZE - 1))
	{muestra=0;}
}



void func_TimerADC(void){  				/* Funcion para la conversion ADC cada 2ms */
	AnalogStartConvertion(); 			/* Iniciar la conversion */
}



uint16_t filtrar(uint16_t anterior, uint16_t actual){				/* Funcion para calcular el valor filtrado */
	uint16_t salida_filtrada;
	salida_filtrada = anterior + ( alfa*(actual-anterior) );
	return salida_filtrada;
}



void func_ADC(void){   												/* Interrupcion por dato convertido  */

	AnalogInputRead(CH1, &valor); 									/* Lectura del canal 1 */
	UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10) ); 			/* Envia el valor sin filtrar a la PC */

	if(filtro){
		valor_filtrado = filtrar(valor_anterior,valor); 			/* Calculo del valor filtrado */
		valor_anterior = valor_filtrado;
	UartSendString(SERIAL_PORT_PC,",");								/* La coma separa las señales */
	UartSendString(SERIAL_PORT_PC, UartItoa(valor_filtrado, 10) );  /* Envia el valor filtrado a la PC */

	}
	UartSendString(SERIAL_PORT_PC,"\r");
}





/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	timer_config my_timerADC={TIMER_A,2,func_TimerADC}; 			/* 2ms por la frecuencia de muestreo de 500Hz */
	timer_config my_timerDAC={TIMER_B,4,func_TimerDAC}; 			/* 4ms por F=250Hz ECG */

	analog_input_config my_adc={CH1,AINPUTS_SINGLE_READ,func_ADC};  /* Lectura de una muestra por vez en canal 1  */
	serial_config my_uart={SERIAL_PORT_PC,115200,leer_Serial};		/* Puerto serie PC. Lectura por entrada de PC */

	/* LLamada a funciones de inicializacion */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(& my_timerADC);		/* Inicializacion del timer, se pasa la direccion de la struct */
	TimerInit(& my_timerDAC); 		/* Inicializacion del timer, se pasa la direccion de la struct */
	AnalogInputInit(&my_adc); 		/* Inicializacion del ADC, se pasa la direccion de la struct */
	UartInit(&my_uart); 			/* Inicializacion del puerto */
	AnalogOutputInit();				/* Inicializacion del DAC */

	/* Se llama a las funciones que permiten interrupciones por switches */
	SwitchActivInt(SWITCH_1, func_Tecla1);
	SwitchActivInt(SWITCH_2, func_Tecla2);
	SwitchActivInt(SWITCH_3, func_Tecla3);
	SwitchActivInt(SWITCH_4, func_Tecla4);

	/* Inicio de la temporizacion de los timers */
	TimerStart(my_timerADC.timer);
	TimerStart(my_timerDAC.timer);


	while(1)
	{


	}
	TimerStop(my_timerADC.timer);
	TimerStop(my_timerDAC.timer);

}

/*==================[end of file]============================================*/


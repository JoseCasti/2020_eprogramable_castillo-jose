/*! @mainpage Odometro (medidor de distancia)
 *
 * \section genDesc General Description
 *
 *El sistema mide la distancia recorrida, y la informa a través de la UART cada un segundo.
 *Para el control del sistema se deben usar las teclas:
 *Tecla 1: Encendido.
 *Tecla 2: Apagado.
 *Tecla 3: Reseteo de cuenta.
 *
 *
 * <a href=" ">Operation Example</a>
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |   2/3/2021 | Document creation		                         |
 * |   2/3/2021	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _EJERCICIO1_H
#define _EJERCICIO1_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERCICIO1_H */


﻿Ejercicio 1
  
Odómetro (medidor de distancia). 

Está implementado usando un sensor infrarrojo TCRT5000 que se debe conectar a la placa EDU-CIAA de la siguiente forma:
   |   TCRT5000		|   EDU-CIAA	|
   |:--------------:|:--------------|
   | 	DOUT		| 	T_COL0		|
   | 	+5V		 	| 	+5V			|
   | 	GND		 	| 	GND			| 
El sistema mide la distancia recorrida, y la informa a través de la UART cada un segundo.
Para el control del sistema se deben usar las teclas:
Tecla 1: Encendido.
Tecla 2: Apagado.
Tecla 3: Reseteo de cuenta. 
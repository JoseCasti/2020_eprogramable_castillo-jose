/*! @mainpage Odometro (medidor de distancia)
 *
 * \section genDesc General Description
 *
 *El sistema mide la distancia recorrida, y la informa a través de la UART cada un segundo.
 *Para el control del sistema se deben usar las teclas:
 *Tecla 1: Encendido.
 *Tecla 2: Apagado.
 *Tecla 3: Reseteo de cuenta.
 *
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   TCRT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT		| 	T_COL0		|
 * | 	+5V		 	| 	+5V			|
 * | 	GND		 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |  2/3/2021  | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Ejercicio1.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "uart.h"
#include "timer.h"
#include "gpio.h"
#include "bool.h"
#include "Tcrt5000.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
/** @def BAUD_RATE
*	@brief Velocidad de transmision de datos en bits/segundo
*/
#define BAUD_RATE 9600



/** @def PERIODO
*	@brief Periodo de muestreo 1000ms
*/
#define PERIODO 1000



/** @def LONG_ABERTURA
*	@brief Longitud de cada abertura y espacio entre aberturas. 2.5cm
*	Como radio=16cm y considerando 20 aberturas y 20 espacios entre ellas.
*/
#define LONG_ABERTURA 2.5









/*==================[internal data definition]===============================*/

float distancia=0.0;		/**< Variable para guardar el valor de la distancia */
bool encendido=FALSE;		/**< Variable para encender o apagar. Se inicializa el dispositivo apagado */
bool estado_actual=FALSE;	/**< Variable para guardar el estado actual del sensor */
bool estado_anterior=FALSE;	/**< Variable para guardar el estado anterior del sensor */






/*==================[internal functions declaration]=========================*/

/** @brief Función para encender el dispositivo
 *
 * 	La funcion permite al pulsar TEC1, encender el dispositivo
 *
 */
void func_Tecla1(void);




/** @brief Función para apagar el dispositivo
 *
 * 	La funcion permite al pulsar TEC2, apagar el dispositivo
 */
void func_Tecla2(void);




/** @brief Función para reiniciar la cuenta
 *
 * 	La funcion permite al pulsar TEC3, reiniciar el valor medido hasta el momento
 *
 */
void func_Tecla3(void);




/** @brief Función para enviar el valor medido a la PC
 *
 * 	La funcion permite enviar el valor medido, en
 * 	determinada unidad de longitud, a la PC mediante la UART
 *
 */
void enviar_Distancia(void);





/** @brief Función para la inicializacion de los modulos del sistema
 *
 * 	La funcion llama a las funciones de inicializacion correspondientes
 *
 */
void init_Sistema(void);












	/* Implementaciones de funciones */

void func_Tecla1(void){
	encendido=TRUE;
}


void func_Tecla2(void){
	encendido=FALSE;
}


void func_Tecla3(void){
	distancia=0.0;					/* Reinicio de cuenta */
}



void enviar_Distancia(void){
	if(encendido){
	UartSendByte(SERIAL_PORT_PC,&distancia);
    UartSendString(SERIAL_PORT_PC, " cm\r\n");
	}
}




void init_Sistema(void){   												/* Inicializacion del sistema */

	timer_config timerADC={TIMER_A,PERIODO,enviar_Distancia};  			/* Envio de distancia medida cada 1000ms */

	serial_config uart={SERIAL_PORT_PC,BAUD_RATE,NULL};					/* Mediante UART solo envio de datos a PC */


	/*Inicializaciones*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	UartInit(&uart); 						/* Inicializacion del puerto */
	TimerInit(&timerADC);					/* Se inicializa el timer */
	TimerStart(timerADC.timer);				/* Se activa el timer */

	SwitchActivInt(SWITCH_1, func_Tecla1); 	/* Llama a funcion para encender dispositivo */
	SwitchActivInt(SWITCH_2, func_Tecla2); 	/* Llama a funcion para apagar dispositivo */
	SwitchActivInt(SWITCH_3, func_Tecla3); 	/* Llama a funcion para reiniciar medidor de distancia */

}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	init_Sistema();


	while(1)
	{
		estado_actual=Tcrt5000State();							/* Se lee estado del sensor */
		if(estado_actual == TRUE && estado_anterior == FALSE)   /* Para detectar una nueva abertura */
			{distancia= distancia + LONG_ABERTURA;}				/* Se suma la longitud de una abertura */

		estado_anterior=estado_actual;

	}

	Tcrt5000Deinit(GPIO_T_COL0);
	TimerStop(TIMER_A);

}

/*==================[end of file]============================================*/


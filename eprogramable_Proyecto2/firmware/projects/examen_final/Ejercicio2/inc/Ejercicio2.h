/*! @mainpage Goniometro digital de rodilla
 *
 * \section genDesc General Description
 *
 *El sistema mide un angulo de 0° a 180º y envia a traves de la UART tanto el valor de ángulo actual
 *como el valor máximo medido hasta el momento, en grados.
 *Para el control del sistema se deben usar las teclas:
 *Tecla 1: Encendido.
 *Tecla 2: Apagado.
 *Tecla 3: Reseteo de la medición.
 *
 *
 *
 * <a href="">Operation Example</a>
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN ext		| 	   3.3V	    |
 * | 	PIN med	 	| 	   CH1  	|
 * | 	PIN ext	 	| 	   GNDA  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |   2/3/2021 | Document creation		                         |
 * |   2/3/2021	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Jose Castillo
 *
 */

#ifndef _EJERCICIO2_H
#define _EJERCICIO2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERCICIO2_H */


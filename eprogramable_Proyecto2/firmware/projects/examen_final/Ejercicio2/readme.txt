﻿Ejercicio 2
  
Goniometro digital de rodilla
 
El sistema utiliza un potenciómetro, entonces se debe realizar la siguiente conexion a la
placa EDU-CIAA:
 |  POTENCIOMETRO |   EDU-CIAA	|
 |:----------- --:|:--------------|
 | 	PIN ext		| 	   3.3V	    |
 | 	PIN med	 	| 	   CH1  	|
 | 	PIN ext	 	| 	   GNDA  	|

El sistema mide un angulo de 0° a 180º y envia a traves de la UART tanto el valor de ángulo actual 
como el valor máximo medido hasta el momento, en grados. 

Para el control del sistema se deben usar las teclas:
Tecla 1: Encendido.
Tecla 2: Apagado.
Tecla 3: Reseteo de la medición.

/*! @mainpage Adquisicion presion arterial
 *
 * \section genDesc General Description
 *
 *El sistema mide un angulo de 0° a 180º y envia a traves de la UART tanto el valor de ángulo actual
 *como el valor máximo medido hasta el momento, en grados.
 *Para el control del sistema se deben usar las teclas:
 *Tecla 1: Encendido.
 *Tecla 2: Apagado.
 *Tecla 3: Reseteo de la medición.
 *
 *
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENCIOMETRO |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN ext		| 	   3.3V	    |
 * | 	PIN med	 	| 	   CH1  	|
 * | 	PIN ext	 	| 	   GNDA  	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * |  2/3/2021  | Document creation		                         |
 * |			|							                     |
 *
 * @author Jose Castillo
 *
 */

/*==================[inclusions]=============================================*/
#include "Ejercicio2.h"       /* <= own header */
#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "gpio.h"


/*==================[macros and definitions]=================================*/
/** @def BAUD_RATE
*	@brief Velocidad de transmision de datos en bits/segundo
*/
#define BAUD_RATE 9600



/** @def PERIODO
*	@brief Periodo de muestreo 1000ms.
*/
#define PERIODO 1000












/*==================[internal data definition]===============================*/
bool encendido=FALSE;	/**< Variable para encender o apagar el dispositivo */
float valor;			/**< Variable para guardar el valor actual medido */
float ang_Max=0.0;		/**< Variable para guardar el valor maximo medido en grados */
float ang;				/**< Variable para guardar el valor actual medido en grados*/







/*==================[internal functions declaration]=========================*/

/** @brief Función para encender el dispositivo
 *
 * 	La funcion permite al pulsar TEC1, encender el dispositivo
 *
 */
void func_Tecla1(void);




/** @brief Función para apagar el dispositivo
 *
 * 	La funcion permite al pulsar TEC2, apagar el dispositivo
 */
void func_Tecla2(void);




/** @brief Función para reiniciar la cuenta
 *
 * 	La funcion permite al pulsar TEC3, reiniciar el valor maximo hasta el momento
 *
 */
void func_Tecla3(void);




/** @brief Función para realizar la conversion
 *
 * 	La funcion llama a la funcion correspondiente para la conversion AD
 * 	y es solicitada cada 1seg por el timer.
 *
 */
void convert(void);



/** @brief Función para la inicializacion de los modulos del sistema
 *
 * 	La funcion llama a las funciones de inicializacion correspondientes
 *
 */
void init_System(void);












	/* Implementaciones de funciones */

void func_Tec1(void){
	encendido=TRUE;
}


void func_Tec2(void){
	encendido=FALSE;
}


void func_Tec3(void){
	ang_Max=0.0;						/* Reinicio de cuenta */
	valor=0.0;
}




void convert(void){  					/* Funcion para la conversion AD */
	AnalogStartConvertion(); 			/* Iniciar la conversion */
}



void read_Value(void){   				/* Interrupcion por dato convertido  */
	AnalogInputRead(CH1, &valor); 		/* Lectura del canal 1, en variable 'valor' */
}




void init_System(void){   												/* Inicializacion del sistema */

	timer_config timerADC={TIMER_A,PERIODO,convert};  					/* Realizar la conversion AD cada 1000ms */

	analog_input_config angulo={CH1,AINPUTS_SINGLE_READ,read_Value};  	/* Lectura de una muestra por vez en canal 1 */

	serial_config uart={SERIAL_PORT_PC,BAUD_RATE,NULL};					/* Mediante UART solo envio de datos a PC */


	/*Inicializaciones*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	AnalogInputInit(&angulo); 				/* Inicializacion del ADC, se pasa la direccion de la struct */
	UartInit(&uart); 						/* Inicializacion del puerto */
	TimerInit(&timerADC);					/* Se inicializa el timer */
	TimerStart(TIMER_A);				/* Se activa el timer */

	SwitchActivInt(SWITCH_1, func_Tec1); 	/* Llama a funcion para encender dispositivo */
	SwitchActivInt(SWITCH_2, func_Tec2); 	/* Llama a funcion para apagar dispositivo */
	SwitchActivInt(SWITCH_3, func_Tec3); 	/* Llama a funcion para reiniciar la medicion */

}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	init_System();


	while(1)
	{

	if(encendido){

	if(valor>ang_Max){
		ang_Max=valor;}

	/* Calculo de angulo en grados para el valor actual y maximo */

	ang= ( valor * 180 )/1024.0;		//Para convertir la palabra en volts, y en grados
	ang_Max= ( ang_Max * 180 )/1024.0;


	/* Envia el valor actual del angulo y valor maximo a la PC */
	UartSendString(SERIAL_PORT_PC, UartItoa(ang, 10) );
	UartSendString(SERIAL_PORT_PC,"° - ");
	UartSendString(SERIAL_PORT_PC, UartItoa(ang_Max, 10) );
	UartSendString(SERIAL_PORT_PC,"°\r\n");

			}
	}

	TimerStop(TIMER_A);
}

/*==================[end of file]============================================*/


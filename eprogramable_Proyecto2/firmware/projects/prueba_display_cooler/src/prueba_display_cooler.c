/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "prueba_display_cooler.h"       /* <= own header */

#include "bool.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "spi.h"
#include "cooler.h"
#include "max7219.h"

/*==================[macros and definitions]=================================*/



/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	spiConfig_t my_spi={SPI_1,MASTER,MODE0, 1000000,SPI_POLLING};
	//uint8_t temperatura=98;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	//CoolerInit(GPIO_1);
	SpiInit(my_spi);
	MAX7219_Init();
	//LedOn(LED_2);
	while(1)
		{
		//MAX7219_DisplayTestStart();
		//MAX7219_DisplayTestStop();
		//MAX7219_Clear ();
		//for(char digit=1 ; digit<4 ; digit++)
		{ MAX7219_DisplayChar('1', '4');
		MAX7219_DisplayChar('2', '5');
		MAX7219_DisplayChar('3', '1');}



		//if(temperatura>100){
			//			CoolerOn();		/* Encender cooler */
				//		LedOn(LED_1);	/* Led de cooler encendido */

		}

		//else {CoolerOff();LedOff(LED_1);}
		//temperatura++;

	SpiDeInit(my_spi.port);
	//CoolerDeinit(GPIO_1);
}

/*==================[end of file]============================================*/

